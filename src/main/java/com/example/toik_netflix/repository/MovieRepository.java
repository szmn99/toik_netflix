package com.example.toik_netflix.repository;

import com.example.toik_netflix.dto.MovieListDto;

public interface MovieRepository {
    MovieListDto getMovies();
}
