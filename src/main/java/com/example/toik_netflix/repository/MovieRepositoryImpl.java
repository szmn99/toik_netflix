package com.example.toik_netflix.repository;

import com.example.toik_netflix.dto.MovieDto;
import com.example.toik_netflix.dto.MovieListDto;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository {
    private MovieListDto movies;

    public MovieRepositoryImpl() {
        List<MovieDto> movieDtos = new ArrayList<>();
        movieDtos.add(new MovieDto(1, "Piraci z krzemowej doliny", 1994, "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"));
        movieDtos.add(new MovieDto(2, "Ja, robot", 2004, "https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg"));
        movieDtos.add(new MovieDto(3, "Kod nieśmiertelności", 2011, "https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg"));
        movieDtos.add(new MovieDto(4, "John Wick", 2014, "https://upload.wikimedia.org/wikipedia/en/9/98/John_Wick_TeaserPoster.jpg"));
        movieDtos.add(new MovieDto(5, "Ex Machina", 2015, "https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg"));
        movies = new MovieListDto(movieDtos);
    }

    @Override
    public MovieListDto getMovies() {
        return movies;
    }
}
