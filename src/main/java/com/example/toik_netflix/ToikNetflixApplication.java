package com.example.toik_netflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToikNetflixApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToikNetflixApplication.class, args);
	}

}
