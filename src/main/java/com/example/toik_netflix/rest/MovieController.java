package com.example.toik_netflix.rest;

import com.example.toik_netflix.dto.MovieListDto;
import com.example.toik_netflix.service.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @CrossOrigin()
    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        return ResponseEntity.ok().body(movieService.getMovies());
    }

}
