package com.example.toik_netflix.service;

import com.example.toik_netflix.dto.MovieListDto;
import com.example.toik_netflix.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public MovieServiceImpl() {
    }

    @Override
    public MovieListDto getMovies() {
        return movieRepository.getMovies();
    }
}
