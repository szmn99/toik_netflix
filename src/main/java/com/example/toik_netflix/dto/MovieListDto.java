package com.example.toik_netflix.dto;

import java.util.List;
import java.util.stream.Collectors;

public class MovieListDto {
    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }

    public MovieListDto() {
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    @Override
    public String toString() {
        return "[" + movies.stream()
                .map(movie -> String.valueOf(movie.getMovieId()))
                .collect(Collectors.joining(",")) + "]";
    }
}
